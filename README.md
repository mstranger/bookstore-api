# bookstore-api

Rails backend for bookstore ember app. Using JSONapi interaction.

## Instalation

* `git clone ...`
* `cd bookstore-api`
* `bundle install`

## Running

* `rails server`
