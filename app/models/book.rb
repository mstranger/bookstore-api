class Book < ApplicationRecord
  # TODO: polimorphic - true
  belongs_to :author
  belongs_to :publisher, polymorphic: true
end
